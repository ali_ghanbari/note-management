const initialState = localStorage.getItem('userName') || ''

const reducers = {
	SET_USER: (_, user) => (user ? user : ''),
	RESET_USER: () => '',
}

export default (state = initialState, { type, payload }) =>
	reducers[type] ? reducers[type](state, payload) : state
