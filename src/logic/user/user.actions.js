import { createAction } from 'redux-actions'
import { dispatch } from '../../setup/redux'

const setUser = createAction('SET_USER')
export const dispatchSetUser = rewards => dispatch(setUser(rewards))

const resetUser = createAction('RESET_USER')
export const dispatchResetUser = () => dispatch(resetUser())
