const initialState = []

const reducers = {
	ADD_NOTE: (state, note) => [...state, note],
	REMOVE_NOTE: (state, removingNoteId) =>
		console.log(state, ' deleting') ||
		state.filter(({ id }) => {
			console.log(id === removingNoteId)
			return id !== removingNoteId
		}),
	EDIT_NOTE: (state, editedNote) =>
		state.map(note => (note.id === editedNote.id ? editedNote : note)),
	SET_NOTES: (_, notes) => (notes ? notes : []),
}

export default (state = initialState, { type, payload }) =>
	reducers[type] ? reducers[type](state, payload) : state
