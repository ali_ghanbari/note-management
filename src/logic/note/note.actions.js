import { createAction } from 'redux-actions'
import { dispatch } from '../../setup/redux'

const addNote = createAction('ADD_NOTE')
export const dispatchAddNote = note => dispatch(addNote(note))

const editNote = createAction('EDIT_NOTE')
export const dispatchEditNote = rewards => dispatch(editNote(rewards))

const removeNote = createAction('REMOVE_NOTE')
export const dispatchRemoveNote = note => dispatch(removeNote(note))

const setNotes = createAction('SET_NOTES')
export const dispatchSetNotes = notes => dispatch(setNotes(notes))
