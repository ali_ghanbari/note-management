// modules
import { combineReducers } from 'redux'

// reducers
import userReducers from '../user/user.reducers'
import notesReducers from '../note/note.reducers'

export default combineReducers({
	user: userReducers,
	notes: notesReducers,
})
