import mainReducers from '../logic/main/main.reducer.js'

export default {
	view: () => ({}),
	main: mainReducers,
}
