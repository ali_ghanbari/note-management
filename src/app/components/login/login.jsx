//modules
import React, { useState } from 'react'

//components
import Input from '../../../helper/components/input/input'

//style
import './login.css'

export default ({ user, onClick }) => {
	const [userName, setUserName] = useState(user)
	return (
		<div className='login center'>
			<div className='login-form '>
				<Input
					title='Name'
					type='row'
					value={userName}
					onChange={setUserName}
				/>

				<button className='login-button' onClick={() => onClick(userName)}>
					Login
				</button>
			</div>
		</div>
	)
}
