// modules
import { connect } from 'react-redux'
import { navigate } from '@reach/router'

// components
import Login from './login'

import { dispatchSetUser } from '../../../logic/user/user.actions'

const mapStateToProps = state => ({
	user: state.main.user,
})

const mapDispatchToProps = () => ({
	onClick: userName => {
		dispatchSetUser(userName)
		localStorage.setItem('userName', userName)
		console.log('user name : ', localStorage.getItem('userName'))
		navigate('/mainPage')
	},
})

export default connect(mapStateToProps, mapDispatchToProps)(Login)
