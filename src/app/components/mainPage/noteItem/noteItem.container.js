// modules
import { connect } from 'react-redux'
import { navigate } from '@reach/router'

//setup
import { getState } from '../../../../setup/redux'

// components
import NoteItem from './noteItem'

import { dispatchRemoveNote } from '../../../../logic/note/note.actions'

const mapDispatchToProps = () => ({
	onRemoveClick: id => {
		console.log('deleteing ', id)
		dispatchRemoveNote(id)

		const newNotes = getState().main.notes
		localStorage.setItem('notes', JSON.stringify(newNotes))
	},
	onEditClick: id => {
		navigate(`/note/${id}`)
	},
})

export default connect(null, mapDispatchToProps)(NoteItem)
