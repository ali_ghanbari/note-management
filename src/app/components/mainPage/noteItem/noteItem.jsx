//modules
import React from 'react'

//style
import './noteItem.css'

export default ({
	id,
	user,
	date,
	title,
	text,
	onRemoveClick,
	onEditClick,
}) => (
	<div className='note-item'>
		<div className='note-item__info'>
			<h4 className='note-item__info-userName center'>{user}</h4>
			<p className='note-item__info-date center'>{date}</p>
		</div>
		<div className='note-item__content'>
			<div className='note-item__content-titleBox'>
				<div className='note-item__content-titleBox-label center' />
				<p className='note-item__content-titleBox-title center'>{title}</p>
			</div>
			<p className='note-item__content-text'>{text}</p>
		</div>
		<div className='note-item__buttons'>
			<img
				className='small-icon'
				alt='edit'
				src='edit.png'
				onClick={() => {
					onEditClick(id)
				}}
			/>
			<img
				className='small-icon'
				alt='delete'
				src='delete.png'
				onClick={() => {
					console.log(id, user, date, title, text)
					onRemoveClick(id)
				}}
			/>
		</div>
	</div>
)
