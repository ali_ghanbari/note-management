//modules
import React from 'react'

//style
import './header.css'

export default () => (
	<div className='header'>
		<h2 className='header-title'>Note Management</h2>
	</div>
)
