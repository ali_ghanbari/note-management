// modules
import { connect } from 'react-redux'
import { navigate } from '@reach/router'

// components
import MainPage from './mainPage'

import { dispatchSetNotes } from '../../../../logic/note/note.actions'

const mapStateToProps = state => ({
	user: state.main.user,
	notes: state.main.notes,
})

const mapDispatchToProps = () => ({
	onMount: () => {
		dispatchSetNotes(JSON.parse(localStorage.getItem('notes') || '[]'))
	},

	onAddClick: () => {
		navigate(`/note/${'new'}`)
	},
})

export default connect(mapStateToProps, mapDispatchToProps)(MainPage)
