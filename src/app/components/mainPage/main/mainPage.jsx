//modules
import React, { useEffect } from 'react'

//components
import Header from '../header/header'
import NoteItem from '../noteItem/noteItem.container'

//style
import './mainPage.css'

export default ({ user, onAddClick, onMount, notes }) => {
	useEffect(() => {
		onMount()
	}, [])

	return notes ? (
		<div className='main-page'>
			<Header />
			<div className='main-page__profile'>
				<div className='main-page__profile-info'>
					<img alt='profile' src='profile.png' className='icon' />
					<p className='main-page__profile-userName'>{user}</p>
				</div>
				<img
					alt='plus'
					className='main-page__profile-add'
					src='add.png'
					onClick={onAddClick}
				/>
			</div>
			<div className='main-page__notes'>
				{notes.map(note => (
					<NoteItem key={note.id} {...note} />
				))}
			</div>
		</div>
	) : null
}
