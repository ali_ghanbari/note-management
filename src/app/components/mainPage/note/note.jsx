//modules
import React, { useState, useEffect } from 'react'

//components
import Header from '../header/header'

//style
import './note.css'

export default ({ user, id, note, onSubmitClick }) => {
	const [title, setTitle] = useState('')
	const [text, setText] = useState('')

	useEffect(() => {
		setTitle(note.title || '')
		setText(note.text || '')
	}, [])

	return (
		<div className='note'>
			<Header />
			<div className='note__fields-box center'>
				<input
					value={title}
					placeholder='Title'
					onChange={e => setTitle(e.target.value)}
					className='note-title'
				></input>
				<textarea
					value={text}
					placeholder='Text'
					onChange={e => setText(e.target.value)}
					className='note-text'
				/>
				<button
					className='note__submit-button'
					onClick={() => {
						const d = new Date()
						const date = d.getMonth() + '/' + d.getDay() + '/' + d.getFullYear()
						onSubmitClick({ date, user, id, title, text })
					}}
				>
					Submit
				</button>
			</div>
		</div>
	)
}
