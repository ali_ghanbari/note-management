// modules
import { connect } from 'react-redux'
import { navigate } from '@reach/router'
import * as R from 'ramda'
// components
import Note from './note'
// setup
import { getState } from '../../../../setup/redux'

import {
	dispatchAddNote,
	dispatchEditNote,
} from '../../../../logic/note/note.actions'

const mapStateToProps = (state, { id: editingNoteId }) => ({
	user: state.main.user,
	note: R.find(({ id }) => editingNoteId === id)(state.main.notes) || {},
})

const mapDispatchToProps = () => ({
	onSubmitClick: ({ user, id, date, title, text }) => {
		if (id !== 'new') dispatchEditNote({ user, id, date, title, text })
		else {
			const newNoteId =
				Math.floor(Math.random() * 10000000000) + 10000000000 + ''
			dispatchAddNote({ user, id: newNoteId, title, text, date })
		}

		const newNotes = getState().main.notes
		localStorage.setItem('notes', JSON.stringify(newNotes))

		alert('done !')
		navigate('/mainPage')
	},
})

export default connect(mapStateToProps, mapDispatchToProps)(Note)
