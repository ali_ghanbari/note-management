import React from 'react'
import { Router } from '@reach/router'

// components
import Login from '../components/login/login.container'
import MainPage from '../components/mainPage/main/mainPage.container'
import Note from '../components/mainPage/note/note.container'

// setup
import { Provider } from 'react-redux'
import store from '../../setup/redux'
import './App.css'

export default () => (
	<Provider store={store}>
		<div className='app'>
			<Router style={{ display: 'contents' }}>
				<Login path='/' />
				<MainPage path='/mainPage' />
				<Note path='/note/:id' />
			</Router>
		</div>
	</Provider>
)
