/** @format */

//module
import React from 'react'

//style
import './input.css'

export default ({ title, type, addr, onChange, value, isPassword }) => (
	<div className={type === 'row' ? 'input-row' : 'input-col'}>
		<p className={`input-title ${type === 'row' ? 'input-title__row' : ''}`}>
			{title}
		</p>
		{addr ? (
			<textarea
				className='input__text-area input-address'
				value={value}
				onChange={e => onChange(e.target.value)}
			></textarea>
		) : (
			<input
				value={value}
				type={isPassword ? 'password' : ''}
				onChange={e => onChange(e.target.value)}
				className='input__text-area'
			></input>
		)}
	</div>
)
